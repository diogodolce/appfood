import mongoose from 'mongoose'

var products_schema = new mongoose.Schema({
    product_name: {
        type:String,
        required:true
    },

    product_desc: {
        type:String,
        required:false
    },

    price:{
        type:Number,
        required:true
    },

    date:{
        type: Date,
        default:Date.now
    },

    owner_id:{
        type:String,
        required:true
    }

})

const Orders = new  mongoose.Schema({

        products: [ products_schema
        ],

        order_desc: {
            type:String,
            required:false
        },

        date:{
            type: Date,
            default:Date.now
        },

        owner_id:{
            type:String,
            required:true
        },

        status_order:{
            type:Number,
            default:1
        }
})

export default mongoose.model('Orders', Orders)