import Orders from '../models/Orders'
export default (req, res) => {
    
    Orders
        .findOneAndDelete(req.params.id)

        .then(()=> res.status(204).json({status:true}))

        .catch(err => res.status(500)
                            .json({status:false,data:{}})
        )

}