import Orders from '../models/Orders'
export default (req, res) => {
    Orders
        .find({})
        .then((orders)  => {
            if(!orders || !orders.length){
                return res.status(404)
                .json({status:false,data:[]})
            }

            return res.status(200)
                    .json({status:true,data:orders})
        })

        .catch(err => res.status(500)
                            .json({status:false,data:[]})

        )
}