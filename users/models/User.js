import mongoose from 'mongoose'

const User = new  mongoose.Schema({
        name: {
            type:String,
            required:true
        },

        pasword:{
            type:Number,
            required:true
        },

        company: {
            type:String,
            required:true
        },

        create:{
            type: Date,
            default:Date.now
        }
})

export default mongoose.model('User', User)