
(function($){
  $(document).ready(function(){

    const listData = function(){
      $.get('http://localhost:3000/products/',function(result){

        $('#list-table tbody').empty()


          if(!result.data.length && !result.status){
              return;
          }


          result.data.forEach(function(product){
              let tmpl = '<tr>'+
              '<td>'+ product.product_name + '</td>'+
              '<td>'+ product.price + '</td>'+
              '<td>'+ product.product_desc + '</td>'
              '</tr>'

              $('#list-table tbody').append(tmpl)
              })
          })
         
    }

  
    const createData = function(){
        let product_name = $ ('input[name="product_name"]').val()
        let price = $ ('input[name="price"]').val()
        let product_desc = $ ('input[name="product_desc"]').val()

        if (!product_name || !price){
            console.log('Invalid data')
            return
        }

        $.post('http://localhost:3000/products/',{product_name:product_name,product_desc:product_desc,price:price},function(result){


          console.log(result)

        $('input[name="product_name"]').val('')
				$('input[name="price"]').val('')
				$('input[name="product_desc"]').val('')


        listData()
      })
		}

      listData()
      $('#btn_create').on('click',createData)
  })
})(jQuery)