import users from './users/index'
import orders from './orders/index'
import hello from './hello/index'
import products from './products/index'
export default (app) => {

    app.use('/',hello)
    app.use('/products', products)
    app.use('/users',users)
    app.use('/orders',orders)
}
