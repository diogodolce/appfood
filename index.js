import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import cors from 'cors'
import routes from './routes.js'


const app = express()
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())
app.use(cors())

let  db  = mongoose.connect('mongodb://localhost:27017/api_dev', {useNewUrlParser: true, useUnifiedTopology: true});
routes(app)
 app.listen(3000,() => {
     console.log('Express server Runing')
 })