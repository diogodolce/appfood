import mongoose from 'mongoose'

const Product = new  mongoose.Schema({
        product_name: {
            type:String,
            required:true
        },

        product_desc: {
            type:String,
            required:false
        },

        price:{
            type:Number,
            required:true
        },

        date:{
            type: Date,
            default:Date.now
        },

        owner_id:{
            type:String,
            required:false
        }
})

export default mongoose.model('Product', Product)