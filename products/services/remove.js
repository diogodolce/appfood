import Products from '../models/Products'
export default (req, res) => {
    
    Products
        .findOneAndDelete(req.params.id)

        .then(()=> res.status(204).json({status:true}))

        .catch(err => res.status(500)
                            .json({status:false,data:{}})
        )

}