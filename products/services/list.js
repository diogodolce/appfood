import Products from '../models/Products'
export default (req, res) => {
    Products
        .find({})
        .then((products)  => {
            if(!products || !products.length){
                return res.status(404)
                .json({status:false,data:[]})
            }

            return res.status(200)
                    .json({status:true,data:products})
        })

        .catch(err => res.status(500)
                            .json({status:false,data:[]})

        )
}